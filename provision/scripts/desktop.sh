#!/bin/bash

# Run upgrade
ansible-playbook /opt/galaxy/upgrade/main.yml -i /opt/galaxy/upgrade/localhost -t upgrade,unattended_upgrades

# Install nginx
ansible-playbook /opt/galaxy/nginx/main.yml -i /opt/galaxy/nginx/localhost -t install_nginx

# Install php
ansible-playbook /opt/galaxy/php-fpm/main.yml -i /opt/galaxy/php-fpm/localhost -t install_php_fpm

# Install percona
ansible-playbook /opt/galaxy/percona/main.yml -i /opt/galaxy/percona/localhost -t percona_repository

# Install whoniks
ansible-playbook /opt/galaxy/whoniks/main.yml -i /opt/galaxy/whoniks/localhost -t network,permissions,configurations
