#!/bin/bash

# Run upgrade
cd /opt/galaxy/upgrade && bash run.sh

# Run bootstrap
#cd /opt/galaxy/bootstrap && bash ./run.sh

# Run nginx
#cd /opt/galaxy/nginx && bash ./run.sh

# Run PHP-FPM
#cd /opt/galaxy/php-fpm && bash ./run.sh

# Run HHVM
#cd /opt/galaxy/hhvm && bash ./run.sh

# Run percona
#cd /opt/galaxy/percona && bash ./run.sh

# Run docker
#cd /opt/galaxy/docker && bash ./run.sh

# Run rancher
#cd /opt/galaxy/rancher && bash ./run.sh

# Run configserver
#cd /opt/galaxy/configserver && bash ./run.sh

# Run ufw
#cd /opt/galaxy/ufw && bash ./run.sh

# Run website
#cd /opt/galaxy/website && bash ./run.sh
