#!/bin/bash

### Compile ansible
#
#export ANSIBLE_VERSION=v2.0.2.0-1
#apt-get update
#apt-get install -y python-paramiko python-yaml python-jinja2 python-httplib2 python-six python-setuptools make git wget sshpass
#git clone https://github.com/ansible/ansible.git --recursive --quiet
#cd ansible && \
#   git checkout tags/"$ANSIBLE_VERSION" && \
#   make && \
#   make install

### Install ansible Ubuntu repository
#
#apt-get install -y software-properties-common
#apt-add-repository -y ppa:ansible/ansible
#apt-get update
#apt-get install -y ansible

### Install ansible Ubuntu
#
apt-get update
apt-get install -y python-pip python-paramiko python-yaml python-jinja2 python-httplib2 python-six python-setuptools make git wget sshpass
pip install ansible

### Ansible configuration
#
# cp /opt/deploy/extra/config/ansible.cfg /etc/ansible/ansible.cfg
